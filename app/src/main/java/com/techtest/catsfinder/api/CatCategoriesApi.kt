package com.techtest.catsfinder.api

import com.techtest.catsfinder.dto.CatCategoryDto
import retrofit2.http.GET

interface CatCategoriesApi {

    @GET("categories")
    suspend fun getCategories(): List<CatCategoryDto>
}
