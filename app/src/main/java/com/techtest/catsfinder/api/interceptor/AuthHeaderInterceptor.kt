package com.techtest.catsfinder.api.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

//Not secure way of handling api key. Just for the sake of simplicity, should be enough for showcase
private const val HEADER_API = "x-api-key"
private const val API_KEY = "c04fe6ec-af71-4269-85be-39eec62b49be"

class AuthHeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (!hasAuthHeader(request)) {
            request = request.newBuilder().addHeader(HEADER_API, API_KEY).build()
        }
        return chain.proceed(request)
    }

    private fun hasAuthHeader(request: Request) =
        !request.header(HEADER_API).isNullOrEmpty()

}
