package com.techtest.catsfinder.api

import com.techtest.catsfinder.dto.CatBreedDto
import retrofit2.http.GET

interface CatBreedsApi {

    @GET("breeds")
    suspend fun getBreeds(): List<CatBreedDto>
}
