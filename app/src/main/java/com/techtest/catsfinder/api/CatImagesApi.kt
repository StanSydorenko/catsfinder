package com.techtest.catsfinder.api

import com.techtest.catsfinder.dto.CatImageDto
import retrofit2.http.GET
import retrofit2.http.Query

interface CatImagesApi {

    @GET("images/search")
    suspend fun getCatImages(
        @Query("limit") pageSize: Int,
        @Query("page") page: Int,
        @Query("category_ids") categoryId: String?,
        @Query("breed_id") breedId: String?
    ): List<CatImageDto>
}
