package com.techtest.catsfinder.ui.fragments.cat

import androidx.lifecycle.*
import com.techtest.catsfinder.entity.CatDetail
import com.techtest.catsfinder.ui.fragments.cat.data.CatDetailsFragmentSingleViewState
import com.techtest.catsfinder.ui.fragments.cat.data.CatDetailsFragmentViewEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val CAT_DETAILS_KEY = "catDetail"

@HiltViewModel
class CatDetailsViewModel @Inject constructor(state: SavedStateHandle) : ViewModel() {

    private val catDetail: CatDetail = state.get(CAT_DETAILS_KEY)!!

    private val _catDetailsViewState = MutableLiveData(catDetail)
    val catDetailsViewState: LiveData<CatDetail> = _catDetailsViewState

    private val _viewEventFlow = Channel<CatDetailsFragmentSingleViewState.LaunchWikipedia>()
    val viewEventFlow = _viewEventFlow.receiveAsFlow()

    fun onViewEvent(event: CatDetailsFragmentViewEvent) {
        when (event){
           is CatDetailsFragmentViewEvent.OnWikiButtonClicked -> {
               catDetail.wikiUrl?.let {
                   viewModelScope.launch {
                       _viewEventFlow.send(CatDetailsFragmentSingleViewState.LaunchWikipedia(it))
                   }
               }
           }
        }
    }
}
