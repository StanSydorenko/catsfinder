package com.techtest.catsfinder.ui.fragments.images

import com.techtest.catsfinder.entity.CatImage

sealed class ImagesListViewEvent {
    data class ItemClicked(val catImage: CatImage) : ImagesListViewEvent()
}
