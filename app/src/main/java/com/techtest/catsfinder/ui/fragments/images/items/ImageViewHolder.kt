package com.techtest.catsfinder.ui.fragments.images.items

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.techtest.catsfinder.R
import com.techtest.catsfinder.databinding.ListItemImageBinding
import com.techtest.catsfinder.ui.fragments.images.ImagesListViewEvent
import com.techtest.catsfinder.ui.fragments.images.ImagesViewEventListener

class ImageViewHolder(
    private val binding: ListItemImageBinding,
    private val viewEventListener: ImagesViewEventListener,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ImageItem) {
        Glide
            .with(binding.root.context)
            .load(item.image.imageUrl)
            .placeholder(R.drawable.ic_baseline_image_24)
            .centerCrop()
            .into(binding.catImageView)

        binding.catBreedNameTextView.text = item.image.breed?.name.orEmpty()
        binding.root.setOnClickListener {
            viewEventListener.onViewEvent(ImagesListViewEvent.ItemClicked(item.image))
        }
        binding.catBreedNameBackgroundView.isGone = item.image.breed == null
    }

    companion object {
        fun create(
            parent: ViewGroup,
            viewEventListener: ImagesViewEventListener
        ): ImageViewHolder {
            val binding = ListItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return ImageViewHolder(binding, viewEventListener)
        }
    }
}
