package com.techtest.catsfinder.ui.fragments.images

import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.techtest.catsfinder.R
import com.techtest.catsfinder.databinding.FragmentImagesBinding
import com.techtest.catsfinder.ui.fragments.images.items.ImagesAdapter
import com.techtest.catsfinder.ui.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

private const val GRID_COLUMN = 2

@AndroidEntryPoint
class ImagesFragment : Fragment(R.layout.fragment_images) {

    private val binding by viewBinding(FragmentImagesBinding::bind)
    private val imagesViewModel: ImagesViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imagesAdapter = ImagesAdapter(imagesViewModel)
        binding.catImagesRecyclerView.apply {
            layoutManager = GridLayoutManager(context, GRID_COLUMN)
            adapter = imagesAdapter
        }
        viewLifecycleOwner.lifecycleScope.launch {
            imagesViewModel.imagesPagingFlow.collectLatest {
                imagesAdapter.submitData(viewLifecycleOwner.lifecycle, it)
            }
        }

        imagesAdapter.addLoadStateListener { loadState ->
            allViewsGone()
            val fullScreenLoading = loadState.refresh == LoadState.Loading
                    && imagesAdapter.itemCount == 0
            val fullScreenError = loadState.refresh is LoadState.Error
                    && imagesAdapter.itemCount == 0
            val loadedEmpty = loadState.refresh is LoadState.NotLoading
                    && imagesAdapter.itemCount == 0
            when {
                fullScreenLoading -> setupLoadingState()
                fullScreenError -> setupErrorState()
                loadedEmpty -> setupEmptyState()
                else -> setupImagesUi()
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                imagesViewModel.viewEventFlow.collectLatest(::launchCatDetailsFragment)
            }
        }
    }

    private fun launchCatDetailsFragment(
        state: ImagesFragmentSingleViewState.LaunchCatDetailsFragment
    ) {
        val launchImageAction = ImagesFragmentDirections.launchCatDetailsFragment(
            catDetail = state.catDetail
        )
        findNavController().navigate(launchImageAction)
    }

    private fun setupErrorState() {
        binding.errorMessageTextView.isVisible = true
        binding.errorMessageTextView.setText(R.string.error_message)
    }

    private fun setupEmptyState() {
        binding.errorMessageTextView.isVisible = true
        binding.errorMessageTextView.setText(R.string.image_empty_message)
    }

    private fun setupLoadingState() {
        binding.loadingProgressBar.isVisible = true
    }

    private fun setupImagesUi() {
        binding.catImagesRecyclerView.isVisible = true
    }

    private fun allViewsGone() {
        binding.apply {
            catImagesRecyclerView.isGone = true
            loadingProgressBar.isGone = true
            errorMessageTextView.isGone = true
        }
    }

}
