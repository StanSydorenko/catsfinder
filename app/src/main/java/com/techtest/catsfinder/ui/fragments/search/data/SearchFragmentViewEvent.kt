package com.techtest.catsfinder.ui.fragments.search.data

sealed interface SearchFragmentViewEvent {

    data class OnCategoryChecked(
        val categoryId: Int, val
        isChecked: Boolean
    ) : SearchFragmentViewEvent

    data class OnBreedChecked(
        val breedId: String,
        val isChecked: Boolean
    ) : SearchFragmentViewEvent

    object OnShowCatsClicked : SearchFragmentViewEvent
}