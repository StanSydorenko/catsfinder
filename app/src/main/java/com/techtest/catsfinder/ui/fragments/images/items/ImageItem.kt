package com.techtest.catsfinder.ui.fragments.images.items

import com.techtest.catsfinder.entity.CatImage

data class ImageItem(val image: CatImage)
