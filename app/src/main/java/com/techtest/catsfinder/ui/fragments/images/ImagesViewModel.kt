package com.techtest.catsfinder.ui.fragments.images

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.techtest.catsfinder.entity.CatDetail
import com.techtest.catsfinder.ui.fragments.images.items.ImageItem
import com.techtest.catsfinder.usecase.GetImagesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val PAGE_SIZE = 20
private const val CATEGORY_ID_KEY = "categoryId"
private const val BREED_ID_KEY = "breedId"

@HiltViewModel
class ImagesViewModel @Inject constructor(
    private val getImagesUseCase: GetImagesUseCase,
    private val state: SavedStateHandle
) : ViewModel(), ImagesViewEventListener {

    val imagesPagingFlow: Flow<PagingData<ImageItem>> = Pager(PagingConfig(PAGE_SIZE)) {
        getImagesUseCase(
            pageSize = PAGE_SIZE,
            categoryId = state.get(CATEGORY_ID_KEY),
            breedId = state.get(BREED_ID_KEY)
        )
    }
        .flow
        .map { data -> data.map { ImageItem(it) } }
        .cachedIn(viewModelScope)

    private val _viewEventFlow = Channel<ImagesFragmentSingleViewState.LaunchCatDetailsFragment>()
    val viewEventFlow = _viewEventFlow.receiveAsFlow()

    override fun onViewEvent(event: ImagesListViewEvent) {
        when (event) {
            is ImagesListViewEvent.ItemClicked -> {
                with(event.catImage) {
                    val catDetail = CatDetail(
                        imageUrl = imageUrl,
                        breedName = breed?.name,
                        temperament = breed?.temperament,
                        energyLevel = breed?.energyLevel,
                        wikiUrl = breed?.wikiUrl
                    )
                    viewModelScope.launch {
                        _viewEventFlow.send(
                            ImagesFragmentSingleViewState.LaunchCatDetailsFragment(catDetail)
                        )
                    }
                }
            }
        }
    }
}
