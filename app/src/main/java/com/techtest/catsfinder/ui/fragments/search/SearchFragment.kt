package com.techtest.catsfinder.ui.fragments.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.techtest.catsfinder.R
import com.techtest.catsfinder.databinding.FragmentSearchBinding
import com.techtest.catsfinder.databinding.SearchChipBinding
import com.techtest.catsfinder.entity.SearchParams
import com.techtest.catsfinder.ui.UiState
import com.techtest.catsfinder.ui.fragments.search.data.SearchFragmentSingleViewState
import com.techtest.catsfinder.ui.fragments.search.data.SearchFragmentViewEvent
import com.techtest.catsfinder.ui.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.fragment_search) {

    private val binding by viewBinding(FragmentSearchBinding::bind)
    private val searchViewModel: SearchViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchViewModel.searchViewState.observe(viewLifecycleOwner) { state ->
            allViewsGone()
            when (state) {
                is UiState.Error -> setupErrorState()
                UiState.Loading -> setupLoadingState()
                is UiState.Success -> setupSearchUi(state.data)
            }
        }
        searchViewModel.selectedCategoryId.observe(viewLifecycleOwner) { categoryId ->
            binding.catCategoriesChipGroup.apply {
                if (categoryId != null) check(categoryId) else clearCheck()
            }
        }
        searchViewModel.selectedBreedId.observe(viewLifecycleOwner) { breedId ->
            binding.catBreedsChipGroup.apply {
                if (breedId != null) check(breedId.hashCode()) else clearCheck()
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                searchViewModel.viewEventFlow.collectLatest(::launchImagesFragment)
            }
        }
    }

    private fun setupErrorState() {
        binding.errorMessageTextView.isVisible = true
        binding.errorMessageTextView.setText(R.string.error_message)
    }

    private fun setupLoadingState() {
        binding.loadingProgressBar.isVisible = true
    }

    private fun setupSearchUi(searchParams: SearchParams) {
        binding.successStateViewGroup.isVisible = true
        binding.catCategoriesChipGroup.apply {
            removeAllViews()
            searchParams.categories.forEach { category ->
                val chipView = createSearchChip(category.id, category.name) { isChecked ->
                    val event = SearchFragmentViewEvent.OnCategoryChecked(category.id, isChecked)
                    searchViewModel.onViewEvent(event)
                }
                addView(chipView)
            }
        }
        binding.catBreedsChipGroup.apply {
            removeAllViews()
            searchParams.breeds.forEach { breed ->
                val chipView = createSearchChip(breed.id.hashCode(), breed.name) { isChecked ->
                    val event = SearchFragmentViewEvent.OnBreedChecked(breed.id, isChecked)
                    searchViewModel.onViewEvent(event)
                }
                addView(chipView)
            }
        }
        binding.showCatsFab.apply {
            isVisible = true
            setOnClickListener {
                searchViewModel.onViewEvent(SearchFragmentViewEvent.OnShowCatsClicked)
            }
        }
        binding.searchParamsScrollView
            .setOnScrollChangeListener { _: NestedScrollView, _, scrollY, _, oldScrollY ->
                val scrollUp = oldScrollY - scrollY < 0
                binding.showCatsFab.apply {
                    if (scrollUp) shrink() else extend()
                }
            }
    }

    private fun createSearchChip(
        chipId: Int,
        name: String,
        onCheckedChange: (Boolean) -> Unit
    ): View {
        return SearchChipBinding.inflate(LayoutInflater.from(context)).root.apply {
            id = chipId
            text = name
            setOnCheckedChangeListener { _, isChecked -> onCheckedChange(isChecked) }
        }
    }

    private fun allViewsGone() {
        binding.apply {
            successStateViewGroup.isGone = true
            loadingProgressBar.isGone = true
            errorMessageTextView.isGone = true
            showCatsFab.isGone = true
        }
    }

    private fun launchImagesFragment(state: SearchFragmentSingleViewState.LaunchImagesFragment) {
        val launchImageAction = SearchFragmentDirections.launchImagesFragment(
            categoryId = state.categoryId?.toString(),
            breedId = searchViewModel.selectedBreedId.value
        )
        findNavController().navigate(launchImageAction)
    }

}