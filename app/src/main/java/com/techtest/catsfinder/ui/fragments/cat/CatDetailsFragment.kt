package com.techtest.catsfinder.ui.fragments.cat

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bumptech.glide.Glide
import com.techtest.catsfinder.R
import com.techtest.catsfinder.databinding.FragmentCatDetailsBinding
import com.techtest.catsfinder.entity.CatDetail
import com.techtest.catsfinder.ui.fragments.cat.data.CatDetailsFragmentViewEvent
import com.techtest.catsfinder.ui.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CatDetailsFragment : Fragment(R.layout.fragment_cat_details) {

    private val binding by viewBinding(FragmentCatDetailsBinding::bind)
    private val catDetailsViewModel: CatDetailsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        catDetailsViewModel.catDetailsViewState.observe(viewLifecycleOwner, ::setupCatDetailsUi)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                catDetailsViewModel.viewEventFlow.collectLatest { event ->
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(event.wikiUrl))
                    startActivity(browserIntent)
                }
            }
        }
    }

    private fun setupCatDetailsUi(catDetail: CatDetail) {
        Glide
            .with(this)
            .load(catDetail.imageUrl)
            .placeholder(R.drawable.ic_baseline_image_24)
            .centerCrop()
            .into(binding.catImageView)
        catDetail.breedName?.let {
            binding.catBreedNameTextView.isVisible = true
            binding.catBreedNameTextView.text = it
        }
        catDetail.temperament?.let {
            binding.catTemperamentTextView.isVisible = true
            binding.catTemperamentTextView.text = it
        }
        catDetail.energyLevel?.let {
            binding.catEnergyLevelTextView.isVisible = true
            binding.catEnergyLevelTextView.text = getString(R.string.cat_energy_level_text, it)
        }
        catDetail.wikiUrl?.let {
            binding.catWikiButton.isVisible = true
            binding.catWikiButton.setOnClickListener {
                catDetailsViewModel.onViewEvent(CatDetailsFragmentViewEvent.OnWikiButtonClicked)
            }
        }
    }
}
