package com.techtest.catsfinder.ui.fragments.images

import com.techtest.catsfinder.entity.CatDetail

sealed interface ImagesFragmentSingleViewState {
    data class LaunchCatDetailsFragment(val catDetail: CatDetail) : ImagesFragmentSingleViewState
}
