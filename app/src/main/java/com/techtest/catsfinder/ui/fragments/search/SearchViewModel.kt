package com.techtest.catsfinder.ui.fragments.search

import androidx.lifecycle.*
import com.techtest.catsfinder.entity.SearchParams
import com.techtest.catsfinder.ui.UiState
import com.techtest.catsfinder.ui.fragments.search.data.SearchFragmentSingleViewState
import com.techtest.catsfinder.ui.fragments.search.data.SearchFragmentViewEvent
import com.techtest.catsfinder.usecase.GetSearchParamsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val getSearchParamsUseCase: GetSearchParamsUseCase
) : ViewModel() {

    private val _searchViewState = MutableLiveData<UiState<SearchParams>>()
    val searchViewState: LiveData<UiState<SearchParams>> = _searchViewState

    private val _viewEventFlow = Channel<SearchFragmentSingleViewState.LaunchImagesFragment>()
    val viewEventFlow = _viewEventFlow.receiveAsFlow()

    private val _selectedCategoryId = MutableLiveData<Int?>()
    val selectedCategoryId: LiveData<Int?> = _selectedCategoryId

    private val _selectedBreedId = MutableLiveData<String?>()
    val selectedBreedId: LiveData<String?> = _selectedBreedId

    init {
        loadSearchParams()
    }

    fun onViewEvent(event: SearchFragmentViewEvent) {
        return when (event) {
            is SearchFragmentViewEvent.OnCategoryChecked -> handleCategoryChecked(event)
            is SearchFragmentViewEvent.OnBreedChecked -> handleBreedChecked(event)
            SearchFragmentViewEvent.OnShowCatsClicked -> handleOnShowCatsClicked()
        }
    }

    private fun handleCategoryChecked(event: SearchFragmentViewEvent.OnCategoryChecked) {
        _selectedCategoryId.apply {
            if (event.isChecked) {
                value = event.categoryId
            } else if (value == event.categoryId && !event.isChecked) {
                value = null
            }
        }
    }

    private fun handleBreedChecked(event: SearchFragmentViewEvent.OnBreedChecked) {
        _selectedBreedId.apply {
            if (event.isChecked) {
                value = event.breedId
            } else if (value == event.breedId && !event.isChecked) {
                value = null
            }
        }
    }

    private fun handleOnShowCatsClicked() {
        viewModelScope.launch {
            _viewEventFlow.send(SearchFragmentSingleViewState.LaunchImagesFragment(
                categoryId = _selectedCategoryId.value,
                breedId = _selectedBreedId.value
            ))
        }
    }

    private fun loadSearchParams() {
        _searchViewState.value = UiState.Loading
        viewModelScope.launch {
            runCatching {
                getSearchParamsUseCase()
            }.onSuccess { it ->
                _searchViewState.value = UiState.Success(it)
            }.onFailure {
                _searchViewState.value = UiState.Error(it)
            }

        }
    }
}