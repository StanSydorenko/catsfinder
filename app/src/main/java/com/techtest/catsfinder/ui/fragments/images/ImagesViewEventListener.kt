package com.techtest.catsfinder.ui.fragments.images

interface ImagesViewEventListener {
    fun onViewEvent(event: ImagesListViewEvent)
}
