package com.techtest.catsfinder.ui.fragments.search.data

sealed interface SearchFragmentSingleViewState {
    data class LaunchImagesFragment(
        val categoryId: Int?,
        val breedId: String?
    ) : SearchFragmentSingleViewState
}
