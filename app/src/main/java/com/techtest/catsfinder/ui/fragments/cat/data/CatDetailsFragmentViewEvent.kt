package com.techtest.catsfinder.ui.fragments.cat.data

sealed interface CatDetailsFragmentViewEvent {

    object OnWikiButtonClicked : CatDetailsFragmentViewEvent
}