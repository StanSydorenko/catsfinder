package com.techtest.catsfinder.ui.fragments.cat.data

sealed interface CatDetailsFragmentSingleViewState {
    data class LaunchWikipedia(val wikiUrl: String) : CatDetailsFragmentSingleViewState
}
