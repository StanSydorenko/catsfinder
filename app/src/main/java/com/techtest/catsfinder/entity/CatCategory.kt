package com.techtest.catsfinder.entity

data class CatCategory(
    val id: Int,
    val name: String
)
