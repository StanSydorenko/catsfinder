package com.techtest.catsfinder.entity

data class CatImage(
    val id: String,
    val imageUrl: String,
    val category: CatCategory?,
    val breed: CatBreed?
)
