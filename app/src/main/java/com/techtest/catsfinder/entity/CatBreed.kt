package com.techtest.catsfinder.entity

data class CatBreed(
    val id: String,
    val name: String,
    val temperament: String?,
    val wikiUrl: String?,
    val energyLevel: Int?,
)
