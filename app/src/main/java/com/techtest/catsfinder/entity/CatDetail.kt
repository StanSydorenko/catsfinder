package com.techtest.catsfinder.entity

import java.io.Serializable

data class CatDetail(
    val imageUrl: String,
    val breedName: String?,
    val temperament: String?,
    val energyLevel: Int?,
    val wikiUrl: String?,
): Serializable
