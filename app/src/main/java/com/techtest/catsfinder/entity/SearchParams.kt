package com.techtest.catsfinder.entity

data class SearchParams(val categories: List<CatCategory>, val breeds: List<CatBreed>)
