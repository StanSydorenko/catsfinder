package com.techtest.catsfinder

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CatsFinderApplication : Application()