package com.techtest.catsfinder.mapper

import com.techtest.catsfinder.dto.CatBreedDto
import com.techtest.catsfinder.entity.CatBreed

class BreedMapper {
    fun asEntity(dto: CatBreedDto): CatBreed = with(dto) {
        CatBreed(
            id = id,
            name = name,
            temperament = temperament,
            wikiUrl = wikiUrl,
            energyLevel = energyLevel
        )
    }
}
