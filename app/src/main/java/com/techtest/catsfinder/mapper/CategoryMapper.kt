package com.techtest.catsfinder.mapper

import com.techtest.catsfinder.dto.CatCategoryDto
import com.techtest.catsfinder.entity.CatCategory

class CategoryMapper {
    fun asEntity(dto: CatCategoryDto): CatCategory = with(dto) {
        CatCategory(id = id, name = name)
    }
}
