package com.techtest.catsfinder.mapper

import com.techtest.catsfinder.dto.CatImageDto
import com.techtest.catsfinder.entity.CatImage

class ImageMapper(
    private val categoryMapper: CategoryMapper,
    private val breedMapper: BreedMapper
) {
    fun asEntity(dto: CatImageDto): CatImage = with(dto) {
        CatImage(
            id = id,
            imageUrl = imageUrl,
            category = categories?.firstOrNull()?.let { categoryMapper.asEntity(it) },
            breed = breeds?.firstOrNull()?.let { breedMapper.asEntity(it) }
        )
    }
}
