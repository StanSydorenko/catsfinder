package com.techtest.catsfinder.usecase

import com.techtest.catsfinder.repository.ImagesPagedSource
import com.techtest.catsfinder.repository.ImagesRepository

class GetImagesUseCase(private val imagesRepository: ImagesRepository) {

    operator fun invoke(pageSize: Int, categoryId: String?, breedId: String?): ImagesPagedSource {
        return ImagesPagedSource(
            imagesRepository = imagesRepository,
            pageSize = pageSize,
            categoryId = categoryId,
            breedId = breedId
        )
    }
}
