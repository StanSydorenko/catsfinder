package com.techtest.catsfinder.usecase

import com.techtest.catsfinder.entity.SearchParams
import com.techtest.catsfinder.repository.BreedsRepository
import com.techtest.catsfinder.repository.CategoriesRepository

class GetSearchParamsUseCase(
    private val categoriesRepository: CategoriesRepository,
    private val breedsRepository: BreedsRepository
) {

    suspend operator fun invoke(): SearchParams {
        val categories = categoriesRepository.getCatCategories()
        val breeds = breedsRepository.getCatBreeds()
        return SearchParams(categories = categories, breeds = breeds)
    }
}
