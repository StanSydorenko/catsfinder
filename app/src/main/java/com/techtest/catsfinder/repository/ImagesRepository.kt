package com.techtest.catsfinder.repository

import com.techtest.catsfinder.api.CatImagesApi
import com.techtest.catsfinder.entity.CatImage
import com.techtest.catsfinder.mapper.ImageMapper

class ImagesRepository(
    private val imagesApi: CatImagesApi,
    private val imageMapper: ImageMapper
) {
    suspend fun getCatImages(
        pageSize: Int,
        page: Int,
        categoryId: String?,
        breedId: String?
    ): List<CatImage> {
        return imagesApi.getCatImages(
                pageSize = pageSize,
                page = page,
                categoryId = categoryId,
                breedId = breedId
            )
            .map { imageMapper.asEntity(it) }
    }
}
