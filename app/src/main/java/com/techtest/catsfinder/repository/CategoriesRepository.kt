package com.techtest.catsfinder.repository

import com.techtest.catsfinder.api.CatCategoriesApi
import com.techtest.catsfinder.entity.CatCategory
import com.techtest.catsfinder.mapper.CategoryMapper

class CategoriesRepository(
    private val categoriesApi: CatCategoriesApi,
    private val categoryMapper: CategoryMapper
) {
    suspend fun getCatCategories(): List<CatCategory> {
        return categoriesApi.getCategories().map { categoryMapper.asEntity(it) }
    }
}
