package com.techtest.catsfinder.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.techtest.catsfinder.entity.CatImage

class ImagesPagedSource(
    private val imagesRepository: ImagesRepository,
    private val pageSize: Int,
    private val categoryId: String?,
    private val breedId: String?
) : PagingSource<Int, CatImage>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CatImage> {
        return try {
            val pageNumber = params.key ?: 0
            val response = imagesRepository.getCatImages(
                pageSize = pageSize,
                page = pageNumber,
                categoryId = categoryId,
                breedId = breedId
            )
            val prevKey = if (pageNumber > 0) pageNumber - 1 else null
            val nextKey = if (response.isNotEmpty()) pageNumber + 1 else null
            LoadResult.Page(
                data = response,
                prevKey = prevKey,
                nextKey = nextKey
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, CatImage>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }
}
