package com.techtest.catsfinder.repository

import com.techtest.catsfinder.api.CatBreedsApi
import com.techtest.catsfinder.entity.CatBreed
import com.techtest.catsfinder.mapper.BreedMapper

class BreedsRepository(
    private val breedsApi: CatBreedsApi,
    private val breedMapper: BreedMapper
) {
    suspend fun getCatBreeds(): List<CatBreed> {
        return breedsApi.getBreeds().map { breedMapper.asEntity(it) }
    }
}
