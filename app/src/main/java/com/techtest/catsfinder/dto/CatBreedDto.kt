package com.techtest.catsfinder.dto

import com.google.gson.annotations.SerializedName

data class CatBreedDto(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("temperament") val temperament: String?,
    @SerializedName("wikipedia_url") val wikiUrl: String?,
    @SerializedName("energy_level") val energyLevel: Int?,
)
