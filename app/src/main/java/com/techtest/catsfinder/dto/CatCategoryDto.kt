package com.techtest.catsfinder.dto

import com.google.gson.annotations.SerializedName

data class CatCategoryDto(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
)
