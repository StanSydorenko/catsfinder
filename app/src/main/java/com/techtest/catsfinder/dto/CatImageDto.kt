package com.techtest.catsfinder.dto

import com.google.gson.annotations.SerializedName

data class CatImageDto(
    @SerializedName("id") val id: String,
    @SerializedName("url") val imageUrl: String,
    @SerializedName("categories") val categories: List<CatCategoryDto>?,
    @SerializedName("breeds") val breeds: List<CatBreedDto>?
)
