package com.techtest.catsfinder.di

import com.techtest.catsfinder.mapper.BreedMapper
import com.techtest.catsfinder.mapper.CategoryMapper
import com.techtest.catsfinder.mapper.ImageMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@InstallIn(ViewModelComponent::class)
@Module
class MapperModule {

    @ViewModelScoped
    @Provides
    fun provideCategoryMapper(): CategoryMapper = CategoryMapper()

    @ViewModelScoped
    @Provides
    fun provideBreedMapper(): BreedMapper = BreedMapper()

    @ViewModelScoped
    @Provides
    fun provideImageMapper(
        categoryMapper: CategoryMapper,
        breedMapper: BreedMapper
    ): ImageMapper {
        return ImageMapper(categoryMapper = categoryMapper, breedMapper = breedMapper)
    }
}
