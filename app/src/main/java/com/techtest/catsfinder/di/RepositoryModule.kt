package com.techtest.catsfinder.di

import com.techtest.catsfinder.api.CatBreedsApi
import com.techtest.catsfinder.api.CatCategoriesApi
import com.techtest.catsfinder.api.CatImagesApi
import com.techtest.catsfinder.mapper.BreedMapper
import com.techtest.catsfinder.mapper.CategoryMapper
import com.techtest.catsfinder.mapper.ImageMapper
import com.techtest.catsfinder.repository.BreedsRepository
import com.techtest.catsfinder.repository.CategoriesRepository
import com.techtest.catsfinder.repository.ImagesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@InstallIn(ViewModelComponent::class)
@Module
class RepositoryModule {

    @ViewModelScoped
    @Provides
    fun provideCategoriesRepository(
        categoriesApi: CatCategoriesApi,
        categoryMapper: CategoryMapper
    ): CategoriesRepository {
        return CategoriesRepository(categoriesApi = categoriesApi, categoryMapper = categoryMapper)
    }

    @ViewModelScoped
    @Provides
    fun provideBreedsRepository(
        breedsApi: CatBreedsApi,
        breedMapper: BreedMapper
    ): BreedsRepository {
        return BreedsRepository(breedsApi = breedsApi, breedMapper = breedMapper)
    }

    @ViewModelScoped
    @Provides
    fun provideImagesRepository(
        imagesApi: CatImagesApi,
        imageMapper: ImageMapper
    ): ImagesRepository {
        return ImagesRepository(imagesApi = imagesApi, imageMapper = imageMapper)
    }
}
