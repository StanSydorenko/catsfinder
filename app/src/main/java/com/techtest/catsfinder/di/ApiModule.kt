package com.techtest.catsfinder.di

import com.techtest.catsfinder.BuildConfig
import com.techtest.catsfinder.api.CatBreedsApi
import com.techtest.catsfinder.api.CatCategoriesApi
import com.techtest.catsfinder.api.CatImagesApi
import com.techtest.catsfinder.api.interceptor.AuthHeaderInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_URL = "https://api.thecatapi.com/v1/"

@InstallIn(SingletonComponent::class)
@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {

        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.addInterceptor(AuthHeaderInterceptor())
        if (BuildConfig.DEBUG) {
            val logger = HttpLoggingInterceptor()
                .apply { level = HttpLoggingInterceptor.Level.BODY }
            clientBuilder.addInterceptor(logger)
        }

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(clientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideCatBreedsApi(retrofit: Retrofit): CatBreedsApi {
        return retrofit.create(CatBreedsApi::class.java)
    }

    @Provides
    fun provideCatCategoriesApi(retrofit: Retrofit): CatCategoriesApi {
        return retrofit.create(CatCategoriesApi::class.java)
    }

    @Provides
    fun provideCatImagesApi(retrofit: Retrofit): CatImagesApi {
        return retrofit.create(CatImagesApi::class.java)
    }
}
