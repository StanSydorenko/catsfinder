package com.techtest.catsfinder.di

import com.techtest.catsfinder.repository.BreedsRepository
import com.techtest.catsfinder.repository.CategoriesRepository
import com.techtest.catsfinder.repository.ImagesRepository
import com.techtest.catsfinder.usecase.GetImagesUseCase
import com.techtest.catsfinder.usecase.GetSearchParamsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@InstallIn(ViewModelComponent::class)
@Module
class UseCaseModule {

    @ViewModelScoped
    @Provides
    fun provideGetSearchParamsUseCase(
        categoriesRepository: CategoriesRepository,
        breedsRepository: BreedsRepository
    ): GetSearchParamsUseCase {
        return GetSearchParamsUseCase(
            categoriesRepository = categoriesRepository,
            breedsRepository = breedsRepository
        )
    }

    @ViewModelScoped
    @Provides
    fun provideGetImagesUseCase(imagesRepository: ImagesRepository): GetImagesUseCase {
        return GetImagesUseCase(imagesRepository = imagesRepository)
    }
}
