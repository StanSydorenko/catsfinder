package com.techtest.catsfinder

import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.techtest.catsfinder.ui.fragments.search.SearchFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.Matchers.not
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class SearchFragmentTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Test
    fun whenUserLaunchesSearchScreen_thenOnlyLoadingShouldBeVisible() {

        launchFragmentInHiltContainer<SearchFragment>()

        onView(withId(R.id.loadingProgressBar)).check(matches(isDisplayed()))
        onView(withId(R.id.errorMessageTextView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.showCatsFab)).check(matches(not(isDisplayed())))
        onView(withId(R.id.catBreedsChipGroup)).check(matches(not(isDisplayed())))
        onView(withId(R.id.catCategoriesChipGroup)).check(matches(not(isDisplayed())))
    }

    @Test
    fun whenUserStaysOnSearchScreen_thenSearchParamsChipsShouldBeVisible() {
        launchFragmentInHiltContainer<SearchFragment>()

        Thread.sleep(2000)

        onView(withId(R.id.loadingProgressBar)).check(matches(not(isDisplayed())))
        onView(withId(R.id.errorMessageTextView)).check(matches(not(isDisplayed())))

        onView(withId(R.id.showCatsFab)).check(matches(isDisplayed()))
        onView(withId(R.id.catBreedsChipGroup)).check(matches(isDisplayed()))
        onView(withId(R.id.catCategoriesChipGroup)).check(matches(isDisplayed()))
    }

    @Test
    fun whenUserClicksOnShowCatsButton_thenLaunchImagesFragment() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())

        launchFragmentInHiltContainer<SearchFragment> {
            navController.setGraph(R.navigation.mobile_navigation)
            Navigation.setViewNavController(requireView(), navController)
        }

        Thread.sleep(2000)

        onView(withId(R.id.showCatsFab)).perform(click())

        assertEquals(navController.currentDestination?.id, R.id.navigation_images)
    }
}

