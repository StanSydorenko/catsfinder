package com.techtest.catsfinder

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.flextrade.kfixture.KFixture
import com.techtest.catsfinder.entity.SearchParams
import com.techtest.catsfinder.ui.UiState
import com.techtest.catsfinder.ui.fragments.search.SearchViewModel
import com.techtest.catsfinder.ui.fragments.search.data.SearchFragmentViewEvent
import com.techtest.catsfinder.usecase.GetSearchParamsUseCase

import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.*
import kotlin.random.Random

@ExperimentalCoroutinesApi
class SearchViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val getSearchParamsUseCaseMock: GetSearchParamsUseCase = mock()
    private val uiStateObserver: Observer<UiState<SearchParams>> = mock()
    private val fixture = KFixture()

    private lateinit var viewModel: SearchViewModel

    @Test
    fun `When view model initialized Then update view state with loading`() {
        testCoroutineRule.testCoroutineDispatcher.pauseDispatcher()
        testCoroutineRule.runBlockingTest {

            createViewModel()
            viewModel.searchViewState.observeForever(uiStateObserver)

            with(argumentCaptor<UiState<SearchParams>>()) {
                verify(uiStateObserver).onChanged(capture())
                assertEquals(UiState.Loading, firstValue)
            }
        }
    }

    @Test
    fun `Given view model initialized When data successfully loaded Then update view state with success`() {
        testCoroutineRule.runBlockingTest {
            whenever(getSearchParamsUseCaseMock()).thenReturn(fixture())
            createViewModel()
            viewModel.searchViewState.observeForever(uiStateObserver)

            with(argumentCaptor<UiState<SearchParams>>()) {
                verify(uiStateObserver).onChanged(capture())
                assert(firstValue is UiState.Success)
            }
        }
    }

    @Test
    fun `Given view model initialized When error happened Then update view state with error`() {
        testCoroutineRule.runBlockingTest {

            val expectedError = IllegalStateException()

            whenever(getSearchParamsUseCaseMock()).thenThrow(expectedError)
            createViewModel()
            viewModel.searchViewState.observeForever(uiStateObserver)

            with(argumentCaptor<UiState<SearchParams>>()) {
                verify(uiStateObserver).onChanged(capture())
                assertEquals(UiState.Error(expectedError), firstValue)
            }
        }
    }

    @Test
    fun `When show cats button clicked Then LaunchImagesFragment view state must be exposed with selected params`() {
        testCoroutineRule.runBlockingTest {
            val expectedCategoryId = Random.nextInt()
            val expectedBreedId = "expectedBreedId"

            createViewModel()
            viewModel.onViewEvent(SearchFragmentViewEvent.OnCategoryChecked(expectedCategoryId, true))
            viewModel.onViewEvent(SearchFragmentViewEvent.OnBreedChecked(expectedBreedId, true))
            viewModel.onViewEvent(SearchFragmentViewEvent.OnShowCatsClicked)

            val actualEvent = viewModel.viewEventFlow.first()
            assertEquals(actualEvent.categoryId, expectedCategoryId)
            assertEquals(actualEvent.breedId, expectedBreedId)
        }
    }

    private fun createViewModel(){
        viewModel = SearchViewModel(getSearchParamsUseCaseMock)
    }
}