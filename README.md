### CatsFinder. Implementation details.

This is the implementation of CatsFinder showcase android app. Here are some comments:

1. The app is written in 100% Kotlin code. Which is the standard of industry  now
2. I've used MVVM presentational pattern with layered architecture (UI - VM - UseCase - Repository - Api). Which is also one of the standards for native android apps
3. I've added a limited amount of unit tests and UI tests just as examples. The layered architecture itself makes the app easy to test. In production of course tests should cover full codebase
4. The API doesn't provide a real search by query, only by some search params like cat category or cat breed which I used in the application
5. Also API is quite limited. E.G. it's not possible to query images of multiple breeds, only a single. And many images itself doesn't have essential info (E.G. cat breed) quite often
6. I've never used Hilt before (we use Koin for DI now) so I've decided to give it a try and spent some time also on reading docs.
7. I've also tried  kotlin's Flow as a potential substitute of more usual LiveData from Android Jetpack

Anyway, that was nice. It's quite interesting to start building apps from scratch sometimes.

Developed with Android Studio Bumblebee | 2021.1.1 Beta 5

Tested on emulator Pixel 3 API 29

No other requirements needed - just clone the repository and build the app

### Screenshots

| Search | Images | Cat Details |
| --------|---------|-------|
| ![Search](https://bitbucket.org/StanSydorenko/catsfinder/raw/961c3c6cf4603852711c9228dbb64c690f7418b0/Screenshot_1.png)   | ![Images](https://bitbucket.org/StanSydorenko/catsfinder/raw/961c3c6cf4603852711c9228dbb64c690f7418b0/Screenshot_2.png)   | ![Cat Details](https://bitbucket.org/StanSydorenko/catsfinder/raw/961c3c6cf4603852711c9228dbb64c690f7418b0/Screenshot_3.png)